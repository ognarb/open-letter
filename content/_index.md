+++
title = "Please do not ship work in progress to users"
description = "Open letter: Do not ship work in progress"
template="page.html"
+++

## An open letter from developers to the wider Linux community.

Please read the letter all the way to the end.  This is aimed at
distributions shipping developer's patches without consulting them
first, breaking the intended user experience of upstream.  This letter
is not aimed at tinkerers playing with their own setup.

We are a group of developers in the Linux ecosystem.  We share the
common goal of producing software that offers a great, stable, and
consistent user experience.  However, this is harmed when software is
shipped too early to users.

We value having all of our work out in the open, as early as possible,
since it
- allows us to get early feedback,
- makes our work visible for what is being worked on to avoid overlap, and
- eases collaboration on our work and/or closely related features.

When incomplete work is public as a merge request, that does not mean
that it is ready to be shipped to end users.  This is especially true
if the merge requests are marked as WIP (Work In Progress), Draft, or
haven't been reviewed by the project maintainers.  Moreover, a
project's primary branch (or WIP/development branches) outside of a
release are similarly not ready for end users, since they may break
underlying APIs.  
In short: **tagged releases of a project are the
only safe option to ship to users.**


### We ask respectfully to consult with developers before shipping anything outside of a tagged release to end users.

Developers put in a lot of work to submit new features and bug fixes
to upstream projects to get feedback.  While these patches are
publicly available, it does not mean distributions should ship them
directly, even if the distribution advertises itself as cutting-edge.

Shipping untagged releases or work-in-progress patches confuses users
since such changes often break expected behavior, or lead to
inconsistencies with other distributions.  This results in project
maintainers spending time supporting users for patches which are still
being refined, slowing down the development and review process.  This
results in unfair and inaccurate public perceptions of a developer's
project, despite it never being the intention of the developers to
distribute unfinished patches broadly to end users.  The peer review
process is there for a reason.  Shipping patches early skips this
process, causing instabilities, security issues, and negative feedback
from users.

### However, if an end user is informed and wants to test patches without broadly distributing them to others, that's fine with us.

We certainly want to encourage end users and distributions to test
patches and report their findings.  This helps developers find and fix
bugs, and increases confidence that their work won't cause issues for
users.  This also helps ensure information end up in the right place:
the upstream merge request rather than a distribution's bug tracker,
forum, and/or social media.  If a distribution wish to ship unreleased
or work in progress patches, we believe it should be opt-in (even
better, avoided entirely). The end user must understand that, rather
than being on the cutting edge, they are in "uncharted territory" and
should expect things to break.  Packaging unfinished work and shipping
it to users who have not explicitly consented is unacceptable.


### If you are a distribution that ship developer's unreleased patches, please reconsider this decision.

Shipping unreviewed patches to your users is reckless.  It unfairly
exposes your users to potential issues, wastes time, and may give the
patch's developer or upstream project a bad reputation.  It
discourages developers from working on new features publicly,
something we care about a lot in the Free and Open Source Software
community.  It also frustrates developers, and in some cases, causes
developers to stop supporting a distribution altogether.  In this
scenario, the end user gets the worst of both worlds: a broken
distribution and developers who are unwilling to help.

### If you think you need to ship a patch right away, please reach out.

There might be cases where a merge request looks like it's fixing an
issue your users are seeing and you want to get it shipped to them as
soon as possible.  If this is the case, please reach out to the
developers to discuss how you can help out to ship it as soon as
possible to your users.

Signed,

{{ signatures() }}

##### *Note: The above list is ordered alphabetically. Even though some of us are distribution maintainers or contributors, these are our personal views as individuals, and not those of the projects or organizations that we are associated with.*

## Sign the letter {#sign}

Are you a developer who contributes to Linux and share the same view?
You can [sign](@/sign.md) this letter too.

## Support us

If you want to signal your support when you submit patches for review,
you can [link](@/_index.md) to this open letter or add the badge to
your merge requests:

|**Badges**                                        |                                               |
|--------------------------------------------------|-----------------------------------------------|
| [svg](dontshipwip.svg), [png](dontshipwip.png)   | ![Don't ship WIP badge - 1](dontshipwip.svg)  |
| [svg](dontshipwip2.svg), [png](dontshipwip2.png) | ![Don't ship WIP badge - 2](dontshipwip2.svg) |
| [mkdn](repo-badge.txt) | [![Don't ship WIP repo badge](https://img.shields.io/badge/Please-Don't%20Ship%20WIP-yellow)](https://dont-ship.it/)
